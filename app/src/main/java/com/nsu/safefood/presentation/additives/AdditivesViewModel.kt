package com.nsu.safefood.presentation.additives

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.food.GetAdditivesUseCase
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.toErrorCode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AdditivesViewModel @Inject constructor(
    private val getAdditivesUseCase: GetAdditivesUseCase
) : ViewModel() {

    private val _additivesList = MutableLiveData<DataStatus<List<Additive>>>()
    val additivesList: LiveData<DataStatus<List<Additive>>> = _additivesList

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _additivesList.value = DataStatus.Error(throwable.toErrorCode())
    }

    fun loadAdditives() {
        _additivesList.value = DataStatus.Loading
        viewModelScope.launch(exceptionHandler) {
            val list = getAdditivesUseCase()
            _additivesList.value = list
        }
    }

}