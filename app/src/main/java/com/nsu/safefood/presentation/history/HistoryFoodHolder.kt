package com.nsu.safefood.presentation.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nsu.safefood.R
import com.nsu.safefood.databinding.HistoryFoodItemBinding
import com.nsu.safefood.domain.Food
import com.nsu.safefood.domain.HistoryFood
import com.nsu.safefood.domain.toFood

class HistoryFoodHolder private constructor(
    private val binding: HistoryFoodItemBinding,
    private val onClick: (Food) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun create(
            layoutInflater: LayoutInflater,
            parent: ViewGroup,
            onClick: (Food) -> Unit
        ): HistoryFoodHolder {
            return HistoryFoodHolder(
                HistoryFoodItemBinding.inflate(layoutInflater, parent, false),
                onClick
            )
        }
    }

    fun bind(historyFood: HistoryFood) {
        binding.apply {
            Glide.with(binding.root)
                .load(historyFood.image)
                .placeholder(R.drawable.ic_food)
                .error(R.drawable.ic_running_with_errors)
                .into(foodImage)
            foodName.text = historyFood.name
            foodRating.rating = historyFood.rating.toFloat()
            scannedDate.text = historyFood.scannedDate
            root.setOnClickListener { onClick(historyFood.toFood()) }
        }
    }

}