package com.nsu.safefood.presentation.additives

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsu.safefood.domain.Additive

class AdditivesAdapter(
    private val onClick: (Additive) -> Unit
) : RecyclerView.Adapter<AdditiveHolder>() {

    var additives: List<Additive> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdditiveHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return AdditiveHolder.create(layoutInflater, parent, onClick)
    }

    override fun onBindViewHolder(holder: AdditiveHolder, position: Int) {
        holder.bind(additives[position])
    }

    override fun getItemCount(): Int {
        return additives.size
    }
}