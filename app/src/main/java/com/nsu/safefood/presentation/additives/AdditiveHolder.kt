package com.nsu.safefood.presentation.additives

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.nsu.safefood.R
import com.nsu.safefood.databinding.AdditiveItemBinding
import com.nsu.safefood.domain.Additive

class AdditiveHolder private constructor(
    private val binding: AdditiveItemBinding,
    private val onClick: (Additive) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    private val resources = binding.root.context.resources

    companion object {
        fun create(
            layoutInflater: LayoutInflater,
            parent: ViewGroup,
            onClick: (Additive) -> Unit
        ): AdditiveHolder {
            return AdditiveHolder(
                binding = AdditiveItemBinding.inflate(layoutInflater, parent, false),
                onClick = onClick
            )
        }
    }

    fun bind(additive: Additive) {
        binding.apply {
            additiveName.text = additive.code
            danger.rating = additive.danger.toFloat()
            val drawable = danger.progressDrawable
            val color = getAdditiveColor(additive.danger)
            drawable?.let { ratingDrawable ->
                color.let {
                    DrawableCompat.setTint(ratingDrawable, it)
                }
            }
            binding.root.setOnClickListener { onClick(additive) }
        }
    }

    private fun getAdditiveColor(danger: Int): Int {
        return when (danger) {
            2 -> {
                ResourcesCompat.getColor(resources, R.color.light_green, null)
            }
            3 -> {
                ResourcesCompat.getColor(resources, R.color.yellow, null)
            }
            4 -> {
                ResourcesCompat.getColor(resources, R.color.orange, null)
            }
            5 -> {
                ResourcesCompat.getColor(resources, R.color.red, null)
            }
            else -> ResourcesCompat.getColor(resources, R.color.green, null)
        }
    }

}