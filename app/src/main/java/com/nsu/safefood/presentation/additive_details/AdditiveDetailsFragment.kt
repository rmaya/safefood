package com.nsu.safefood.presentation.additive_details

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.nsu.safefood.R
import com.nsu.safefood.databinding.AdditiveDetailsFragmentBinding
import com.nsu.safefood.domain.Food
import com.nsu.safefood.presentation.MainActivity
import com.nsu.safefood.presentation.food_details.TAG
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.ErrorCodeProcessor
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AdditiveDetailsFragment : Fragment(R.layout.additive_details_fragment) {

    private val viewModel: AdditiveDetailsViewModel by viewModels()
    private val binding: AdditiveDetailsFragmentBinding by viewBinding()
    private val args: AdditiveDetailsFragmentArgs by navArgs()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).setToolbar(binding.additiveAppBar)
        setHasOptionsMenu(true)

        bindInfo()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.empty_app_bar_menu, menu)
    }


    private fun bindInfo() {
        val info = args.additiveInfo
        binding.apply {
            cardViewAdditiveLayout.additiveName.text = info.code
            if (info.general.isNotEmpty()){
                cardViewAdditiveLayout.general.text = info.general
            } else {
                cardViewAdditiveLayout.general.text = getString(R.string.no_info_additive)
            }

            cardViewAdditiveLayout.danger.rating = info.danger.toFloat()
            val drawable = cardViewAdditiveLayout.danger.progressDrawable
            val color = getAdditiveColor(info.danger)
            drawable?.let { ratingDrawable ->
                color.let {
                    DrawableCompat.setTint(ratingDrawable, it)
                }
            }

            if (info.benefit.isNotEmpty()){
                cardViewAdditiveLayout.benefit.text = info.benefit
            } else {
                cardViewAdditiveLayout.benefit.visibility = View.GONE
                cardViewAdditiveLayout.benefitLabel.visibility = View.GONE
            }

            if (info.harm.isNotEmpty()){
                cardViewAdditiveLayout.harm.text = info.harm
            } else {
                cardViewAdditiveLayout.harm.visibility = View.GONE
                cardViewAdditiveLayout.harmLabel.visibility = View.GONE
            }
        }
    }

    private fun getAdditiveColor(danger: Int): Int {
        return when (danger) {
            2 -> {
                ResourcesCompat.getColor(resources, R.color.light_green, null)
            }
            3 -> {
                ResourcesCompat.getColor(resources, R.color.yellow, null)
            }
            4 -> {
                ResourcesCompat.getColor(resources, R.color.orange, null)
            }
            5 -> {
                ResourcesCompat.getColor(resources, R.color.red, null)
            }
            else -> ResourcesCompat.getColor(resources, R.color.green, null)
        }
    }
}