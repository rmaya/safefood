package com.nsu.safefood.presentation.home_list

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.mlkit.vision.common.InputImage
import com.nsu.safefood.BuildConfig
import com.nsu.safefood.R
import com.nsu.safefood.databinding.RecommendListFragmentBinding
import com.nsu.safefood.domain.Food
import com.nsu.safefood.domain.HistoryFood
import com.nsu.safefood.domain.scanning.BarcodeInfo
import com.nsu.safefood.presentation.MainActivity
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.ErrorCodeProcessor
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class RecommendListFragment : Fragment(R.layout.recommend_list_fragment) {

    private val binding: RecommendListFragmentBinding by viewBinding()
    private val viewModel: RecommendListViewModel by viewModels()

    private val recommendationsAdapter = RecommendListAdapter {
        navigateToFoodDetails(it)
    }

    companion object {
        const val TAG = "HomeListFragment"
    }

    @Inject
    lateinit var errorCodeProcessor: ErrorCodeProcessor

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { resultMap ->
        if (resultMap.keys.all { true }) {
            takePictureLauncher.launch(imageUri)
        } else {
            showRationaleDialog()
        }
    }

    private lateinit var imageUri: Uri
    private val takePictureLauncher = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { success ->
        if (success) {
            //navigateToFoodDetails("Fantola оранж кола")
            viewModel.scanImage(InputImage.fromFilePath(requireContext(), imageUri))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).setToolbar(binding.homeListAppBar)
        setHasOptionsMenu(true)

        imageUri = createImageUri()

        initListeners()
        initObservers()

        viewModel.loadRecommendations()
    }

    private fun initObservers(){
        viewModel.barcode.observe(viewLifecycleOwner) {
            processBarCode(it)
        }
        viewModel.recommendations.observe(viewLifecycleOwner){
            processRecommendations(it)
        }
    }

    private fun processRecommendations(status: DataStatus<List<Food>>){
        when (status){
            is DataStatus.Loading -> {
                binding.progressBar.visibility = View.VISIBLE
                binding.placeholder.visibility = View.INVISIBLE
            }
            is DataStatus.Success -> {
                binding.progressBar.visibility = View.INVISIBLE
                binding.placeholder.visibility = View.VISIBLE
                status.data?.let {
                    if (it.isNotEmpty()){
                        recommendationsAdapter.recommendations = it
                        binding.placeholder.visibility = View.INVISIBLE
                        binding.listRecommendations.visibility = View.VISIBLE
                    }
                }
            }
            is DataStatus.Error -> {
                status.code.let {
                    val message = errorCodeProcessor.processErrorCode(it)
                    showMessage(message)
                }
            }
        }
    }

    private fun initListeners() {
        binding.apply {
            listRecommendations.adapter = recommendationsAdapter
            scanButton.setOnClickListener {
                checkPermissionGranted()
            }
            searchFood.setOnEditorActionListener { _, actionId, _ ->
                return@setOnEditorActionListener if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    navigateToFoodDetails(searchFood.text.toString())
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun checkPermissionGranted() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED &&
            (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED)
        ) {
            takePictureLauncher.launch(imageUri)
        } else {
            requestPermissionLauncher.launch(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
            )
        }
    }

    private fun showRationaleDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.scanning_permissions)
        builder.setMessage(R.string.scanning_permissions_body)
        val icon = ResourcesCompat.getDrawable(resources, R.drawable.ic_camera, null)
        val color = ResourcesCompat.getColor(resources, R.color.teal_700, null)
        icon?.setTint(color)
        builder.setIcon(icon)
        builder.setPositiveButton(R.string.yes) { dialog, _ ->
            requestPermissionLauncher.launch(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
            )
            dialog.cancel()
        }
        builder.setNeutralButton(R.string.no) { dialog, _ ->
            dialog.cancel()
        }
        builder.show()
    }

    private fun processBarCode(status: DataStatus<BarcodeInfo>) {
        when (status) {
            is DataStatus.Loading -> {
                binding.progressBar.visibility = View.VISIBLE
            }
            is DataStatus.Success -> {
                binding.progressBar.visibility = View.INVISIBLE
                Log.e(TAG, "Your data: ${status.data}")
                status.data?.let { barcode ->
                    barcode.description?.let { descr ->
                        showMessage("Successfully!")
                        navigateToFoodDetails(descr)
                    }
                }
            }
            is DataStatus.Error -> {
                binding.progressBar.visibility = View.INVISIBLE
                val message = errorCodeProcessor.processErrorCode(status.code)
                showMessage(message)
            }
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    private fun navigateToFoodDetails(name: String) {
        val action =
            RecommendListFragmentDirections.actionHomeListFragmentToFoodDetailsFragment(name = name)
        findNavController().navigate(action)
    }

    private fun navigateToFoodDetails(food: Food) {
        val action =
            RecommendListFragmentDirections.actionHomeListFragmentToFoodDetailsFragment(food = food)
        findNavController().navigate(action)
    }

    private fun createImageUri(): Uri {
        val timeStamp = SimpleDateFormat.getDateTimeInstance().format(Date())
        val storageDir = requireActivity().cacheDir

        val file = File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            createNewFile()
            deleteOnExit()
        }
        return FileProvider.getUriForFile(
            requireContext(),
            "${BuildConfig.APPLICATION_ID}.provider",
            file
        )
    }


}