package com.nsu.safefood.presentation.additive_details

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AdditiveDetailsViewModel @Inject constructor(): ViewModel() {

}