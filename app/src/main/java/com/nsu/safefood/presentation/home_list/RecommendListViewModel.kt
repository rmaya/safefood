package com.nsu.safefood.presentation.home_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import com.nsu.safefood.domain.Food
import com.nsu.safefood.domain.food.GetRecommendationsUseCase
import com.nsu.safefood.domain.history_food.GetFavoriteCategoriesUseCase
import com.nsu.safefood.domain.scanning.BarcodeInfo
import com.nsu.safefood.domain.scanning.GetBarcodeInfoUseCase
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.SingleLiveEvent
import com.nsu.safefood.utils.errors_processing.ErrorCode
import com.nsu.safefood.utils.errors_processing.toErrorCode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecommendListViewModel @Inject constructor(
    private val getBarcodeInfoUseCase: GetBarcodeInfoUseCase,
    private val getRecommendationsUseCase: GetRecommendationsUseCase,
    private val getFavoriteCategoriesUseCase: GetFavoriteCategoriesUseCase
) : ViewModel() {

    private var barcodeScanner: BarcodeScanner = BarcodeScanning.getClient()

    private var _barcode = SingleLiveEvent<DataStatus<BarcodeInfo>>()
    val barcode: LiveData<DataStatus<BarcodeInfo>> = _barcode

    private val _recommendations = MutableLiveData<DataStatus<List<Food>>>()
    val recommendations: LiveData<DataStatus<List<Food>>> = _recommendations

    private val exceptionHandlerBarcode = CoroutineExceptionHandler { _, throwable ->
        _barcode.value = DataStatus.Error(throwable.toErrorCode())
    }

    private val exceptionHandlerRecommend = CoroutineExceptionHandler { _, throwable ->
        _recommendations.value = DataStatus.Error(throwable.toErrorCode())
    }

    fun loadRecommendations(){
        _recommendations.value = DataStatus.Loading
        viewModelScope.launch (exceptionHandlerRecommend) {
            val status = getFavoriteCategoriesUseCase()
            if (status is DataStatus.Success){
                val favCategories = status.data
                favCategories?.let {
                    val list = getRecommendationsUseCase(it)
                    _recommendations.value = list
                }
            }
        }
    }

    fun scanImage(inputImage: InputImage) {
        _barcode.value = DataStatus.Loading
        viewModelScope.launch(exceptionHandlerBarcode) {
            barcodeScanner.process(inputImage)
                .addOnSuccessListener {
                    if (it.size == 0) {
                        _barcode.value = DataStatus.Error(ErrorCode.NO_BAR_CODE)
                    } else {
                        for (barcode in it) {
                            val rawValue = barcode.rawValue
                            rawValue?.toLong()?.let { it1 -> getBarCodeInfo(it1) }
                                ?: DataStatus.Error(ErrorCode.NO_BAR_CODE_INFO)
                        }
                    }
                }
                .addOnFailureListener {
                    _barcode.value = DataStatus.Error(ErrorCode.CAN_NOT_PROCESS_BARCODE)
                }
        }
    }

    private fun getBarCodeInfo(code: Long) {
        viewModelScope.launch(exceptionHandlerBarcode) {
            val barcodeInfo = getBarcodeInfoUseCase(code)
            _barcode.value = barcodeInfo
        }
    }
}