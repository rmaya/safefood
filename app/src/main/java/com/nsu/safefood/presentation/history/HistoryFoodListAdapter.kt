package com.nsu.safefood.presentation.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsu.safefood.domain.Food
import com.nsu.safefood.domain.HistoryFood

class HistoryFoodListAdapter(
    private val onClick: (Food) -> Unit
) : RecyclerView.Adapter<HistoryFoodHolder>() {

    var historyFoodList: List<HistoryFood> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryFoodHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return HistoryFoodHolder.create(layoutInflater, parent, onClick)
    }

    override fun onBindViewHolder(holder: HistoryFoodHolder, position: Int) {
        holder.bind(historyFoodList[position])
    }

    override fun getItemCount(): Int {
        return historyFoodList.size
    }
}