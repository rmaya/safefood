package com.nsu.safefood.presentation.additives

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.nsu.safefood.R
import com.nsu.safefood.databinding.AdditivesFragmentBinding
import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.scanning.BarcodeInfo
import com.nsu.safefood.presentation.MainActivity
import com.nsu.safefood.presentation.home_list.RecommendListFragment
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.ErrorCodeProcessor
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val TAG = "AdditivesFragment"

@AndroidEntryPoint
class AdditivesFragment : Fragment(R.layout.additives_fragment) {

    private val binding: AdditivesFragmentBinding by viewBinding()
    private val viewModel: AdditivesViewModel by viewModels()
    private val additivesAdapter = AdditivesAdapter {
        navigateToDetails(it)
    }

    @Inject
    lateinit var errorCodeProcessor: ErrorCodeProcessor

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as MainActivity).setToolbar(binding.additivesAppBar)
        setHasOptionsMenu(true)

        viewModel.loadAdditives()

        initObservers()
        initListeners()
    }

    private fun initListeners(){
        binding.apply {
            additivesList.adapter = additivesAdapter
        }
    }

    private fun initObservers(){
        viewModel.additivesList.observe(viewLifecycleOwner){
            processAdditivesList(it)
        }
    }

    private fun processAdditivesList(status: DataStatus<List<Additive>>) {
        when (status) {
            is DataStatus.Loading -> {
                binding.progressBar.visibility = View.VISIBLE
            }
            is DataStatus.Success -> {
                binding.progressBar.visibility = View.INVISIBLE
                status.data?.let { list ->
                    Log.i(TAG, "Success! List: $list")
                    additivesAdapter.additives = list
                }
            }
            is DataStatus.Error -> {
                binding.progressBar.visibility = View.INVISIBLE
                val message = errorCodeProcessor.processErrorCode(status.code)
                showMessage(message)
            }
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    private fun navigateToDetails(additive: Additive){
        val action = AdditivesFragmentDirections.actionAdditivesListFragmentToAdditiveDetailsFragment(additive)
        findNavController().navigate(action)
    }

}