package com.nsu.safefood.presentation.home_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nsu.safefood.R
import com.nsu.safefood.databinding.FoodItemBinding
import com.nsu.safefood.domain.Food

class RecommendHolder private constructor(
    private val binding: FoodItemBinding,
    private val onClick: (Food) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun create(
            layoutInflater: LayoutInflater,
            parent: ViewGroup,
            onClick: (Food) -> Unit
        ): RecommendHolder {
            return RecommendHolder(
                binding = FoodItemBinding.inflate(layoutInflater, parent, false),
                onClick = onClick
            )
        }
    }

    fun bind(recommendFood: Food) {
        binding.apply {
            Glide.with(binding.root)
                .load(recommendFood.imageUrl)
                .placeholder(R.drawable.ic_food)
                .error(R.drawable.ic_running_with_errors)
                .into(foodImage)

            foodName.text = recommendFood.name
            foodRating.rating = recommendFood.rating.toFloat()
            root.setOnClickListener { onClick(recommendFood) }
        }
    }
}
