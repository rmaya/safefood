package com.nsu.safefood.presentation.food_details

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.nsu.safefood.R
import com.nsu.safefood.databinding.FoodDetailsFragmentBinding
import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.Food
import com.nsu.safefood.presentation.MainActivity
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.ErrorCodeProcessor
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val TAG = "FoodDetailsFragment"

@AndroidEntryPoint
class FoodDetailsFragment : Fragment(R.layout.food_details_fragment) {

    private val binding: FoodDetailsFragmentBinding by viewBinding()
    private val viewModel: FoodDetailsViewModel by viewModels()
    private val args: FoodDetailsFragmentArgs by navArgs()

    @Inject
    lateinit var errorCodeProcessor: ErrorCodeProcessor


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).setToolbar(binding.foodAppBar)
        setHasOptionsMenu(true)

        bindFoodInfo()
        initObservers()
    }

    private fun bindFoodInfo(){
        args.name?.let {
            viewModel.getFoodInfo(it)
            return
        }
        args.food?.let {
            binding.foodInfoLayout.visibility = View.VISIBLE
            bindInfo(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.empty_app_bar_menu, menu)
    }

    private fun initObservers() {
        viewModel.foodInfo.observe(viewLifecycleOwner) {
            processData(it)
        }
    }

    private fun processData(status: DataStatus<Food>) {
        when (status) {
            is DataStatus.Loading -> {
                binding.progressBar.visibility = View.VISIBLE
            }
            is DataStatus.Success -> {
                binding.progressBar.visibility = View.INVISIBLE
                binding.foodInfoLayout.visibility = View.VISIBLE
                status.data?.let {
                    bindInfo(it)
                    viewModel.saveHistory()
                }
            }
            is DataStatus.Error -> {
                binding.progressBar.visibility = View.INVISIBLE
                Log.i(TAG, "Error! ${status.code}")
                val message = errorCodeProcessor.processErrorCode(status.code)
                showMessage(message)
                navigateUp()
            }
        }
    }

    private fun bindInfo(food: Food) {
        binding.apply {
            Glide.with(this@FoodDetailsFragment)
                .load(food.imageUrl)
                .placeholder(R.drawable.ic_food)
                .error(R.drawable.ic_running_with_errors)
                .into(foodImage)
            title.text = food.name
            cardViewLayout.ingredients.text = food.ingredients
            cardViewLayout.additives.text = processAdditives(food.additives)
            cardViewLayout.ratingBar.rating = food.rating.toFloat()
            cardViewLayout.ratingNumber.text = food.rating.toString()
        }
    }

    private fun processAdditives(list: List<Additive>): Spannable {
        val additives = SpannableStringBuilder()
        list.forEach {
            val spannable = SpannableStringBuilder(it.code)
            when (it.danger) {
                0 -> {
                    spannable.setColoredSpan(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.black,
                            null
                        )
                    )
                }
                1 -> {
                    spannable.setColoredSpan(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.green,
                            null
                        )
                    )
                }
                2 -> {
                    spannable.setColoredSpan(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.light_green,
                            null
                        )
                    )
                }
                3 -> {
                    spannable.setColoredSpan(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.yellow,
                            null
                        )
                    )
                }
                4 -> {
                    spannable.setColoredSpan(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.orange,
                            null
                        )
                    )
                }
                5 -> {
                    spannable.setColoredSpan(ResourcesCompat.getColor(resources, R.color.red, null))
                }
            }
            additives.append(spannable)
            val additiveInfo = SpannableStringBuilder(" (" + it.name + ");\n")
            additiveInfo.setColoredSpan(
                ResourcesCompat.getColor(
                    resources,
                    R.color.black,
                    null
                )
            )
            additives.append(additiveInfo)
        }
        return additives
    }

    private fun SpannableStringBuilder.setColoredSpan(color: Int) {
        this.setSpan(
            ForegroundColorSpan(color),
            0,
            this.length,
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )
    }

    private fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    private fun navigateUp() {
        findNavController().navigateUp()
    }

}