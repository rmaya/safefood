package com.nsu.safefood.presentation.food_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nsu.safefood.domain.Food
import com.nsu.safefood.domain.food.GetFoodInfoUseCase
import com.nsu.safefood.domain.history_food.SaveFoodUseCase
import com.nsu.safefood.domain.toHistoryFood
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.toErrorCode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FoodDetailsViewModel @Inject constructor(
    private val saveFoodUseCase: SaveFoodUseCase,
    private val getFoodInfoUseCase: GetFoodInfoUseCase
): ViewModel() {

    private val _foodInfo = MutableLiveData<DataStatus<Food>>()
    val foodInfo: LiveData<DataStatus<Food>> = _foodInfo

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _foodInfo.value = DataStatus.Error(throwable.toErrorCode())
    }

    fun getFoodInfo(name: String){
        _foodInfo.value = DataStatus.Loading
        viewModelScope.launch (exceptionHandler){
            val foodInfo = getFoodInfoUseCase(name = name)
            _foodInfo.value = foodInfo
        }
    }

    fun saveHistory(){
        viewModelScope.launch {
            val food = (_foodInfo.value as DataStatus.Success).data
            food?.let {
                saveFoodUseCase(it.toHistoryFood())
            }
        }
    }
}