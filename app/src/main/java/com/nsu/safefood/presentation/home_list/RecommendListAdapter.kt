package com.nsu.safefood.presentation.home_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsu.safefood.domain.Food

class RecommendListAdapter(
    private val onClick: (Food) -> Unit
) : RecyclerView.Adapter<RecommendHolder>() {

    var recommendations: List<Food> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return RecommendHolder.create(layoutInflater, parent, onClick)
    }

    override fun onBindViewHolder(holder: RecommendHolder, position: Int) {
        holder.bind(recommendations[position])
    }

    override fun getItemCount(): Int {
        return recommendations.size
    }

}