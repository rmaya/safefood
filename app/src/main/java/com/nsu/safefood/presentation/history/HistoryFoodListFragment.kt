package com.nsu.safefood.presentation.history

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.nsu.safefood.R
import com.nsu.safefood.databinding.HistoryFoodListFragmentBinding
import com.nsu.safefood.domain.Food
import com.nsu.safefood.domain.HistoryFood
import com.nsu.safefood.presentation.MainActivity
import com.nsu.safefood.presentation.home_list.RecommendListFragmentDirections
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.ErrorCodeProcessor
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HistoryFoodListFragment : Fragment(R.layout.history_food_list_fragment) {

    private val viewModel: HistoryFoodListViewModel by viewModels()
    private val binding: HistoryFoodListFragmentBinding by viewBinding()
    private val historyListAdapter = HistoryFoodListAdapter {
        navigateToFoodDetails(it)
    }
    @Inject
    lateinit var errorCodeProcessor: ErrorCodeProcessor


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as MainActivity).setToolbar(binding.historyListAppBar)
        setHasOptionsMenu(true)

        viewModel.loadHistory()
        initObservers()

        binding.apply {
            historyList.adapter = historyListAdapter
        }
    }

    private fun initObservers(){
        viewModel.historyFoodList.observe(viewLifecycleOwner){
            processHistory(it)
        }
    }

    private fun processHistory(status: DataStatus<List<HistoryFood>>){
        when (status){
            is DataStatus.Loading -> {
                binding.progressBar.visibility = View.VISIBLE
            }
            is DataStatus.Success -> {
                binding.progressBar.visibility = View.INVISIBLE
                status.data?.let {
                    if (it.isEmpty()) return@let
                    historyListAdapter.historyFoodList = it
                    binding.placeholder.visibility = View.INVISIBLE
                    binding.historyList.visibility = View.VISIBLE
                }
            }
            is DataStatus.Error -> {
                status.code.let {
                    val message = errorCodeProcessor.processErrorCode(it)
                    showMessage(message)
                }
            }
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    private fun navigateToFoodDetails(food: Food) {
        val action =
            HistoryFoodListFragmentDirections.actionHistoryListFragmentToFoodDetailsFragment(food = food)
        findNavController().navigate(action)
    }

}