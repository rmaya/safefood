package com.nsu.safefood.presentation.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nsu.safefood.domain.HistoryFood
import com.nsu.safefood.domain.history_food.GetHistoryFoodUseCase
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.errors_processing.toErrorCode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HistoryFoodListViewModel @Inject constructor(
    private val getHistoryFoodUseCase: GetHistoryFoodUseCase
) : ViewModel() {

    private val _historyFoodList = MutableLiveData<DataStatus<List<HistoryFood>>>()
    val historyFoodList: LiveData<DataStatus<List<HistoryFood>>> = _historyFoodList

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _historyFoodList.value = DataStatus.Error(throwable.toErrorCode())
        throwable.printStackTrace()
    }

    fun loadHistory() {
        _historyFoodList.value = DataStatus.Loading
        viewModelScope.launch(exceptionHandler) {
            val list = getHistoryFoodUseCase()
            _historyFoodList.value = list
        }
    }
}