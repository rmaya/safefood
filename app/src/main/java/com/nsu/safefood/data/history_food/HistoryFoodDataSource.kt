package com.nsu.safefood.data.history_food

interface HistoryFoodDataSource {

    suspend fun saveFood(historyFoodEntity: HistoryFoodEntity)

    suspend fun getHistoryFood(): List<HistoryFoodEntity>

    suspend fun deleteHistoryFood(historyFoodEntity: HistoryFoodEntity)

    suspend fun getCategoriesWithCount(): List<CategoryEntity>
}