package com.nsu.safefood.data.scanning

import com.nsu.safefood.domain.scanning.BarcodeInfo
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BarcodeRemoteDataSource @Inject constructor(private val barcodeApi: BarcodeApi) :
    BarcodeDataSource {

    override suspend fun getBarcodeInfo(code: Long): Response<BarcodeInfo> =
        barcodeApi.getBarcodeInfo(code)
}