package com.nsu.safefood.data

import java.io.IOException

class NoConnectivityException : IOException()

class NoInternetException : IOException()