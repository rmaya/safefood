package com.nsu.safefood.data.food

import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.Food
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FoodDataSourceImpl @Inject constructor(
    private val foodApi: FoodApi
) : FoodDataSource {

    override suspend fun getFoodInfo(name: String): Response<Food> {
        return foodApi.getFoodInfo(name = name)
    }

    override suspend fun getAdditives(): Response<List<Additive>> {
        return foodApi.getAdditives()
    }

    override suspend fun getRecommendations(categories: List<String>): Response<List<Food>> {
        return foodApi.getRecommendations(categories)
    }
}