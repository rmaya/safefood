package com.nsu.safefood.data.history_food

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [HistoryFoodEntity::class], exportSchema = false, version = 3)
@TypeConverters(Converters::class)
abstract class HistoryFoodDatabase : RoomDatabase(){
    abstract fun getHistoryFoodDao(): HistoryFoodDao
}