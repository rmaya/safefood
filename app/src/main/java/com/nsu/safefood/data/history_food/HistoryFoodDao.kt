package com.nsu.safefood.data.history_food

import androidx.room.*

@Dao
interface HistoryFoodDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun saveFood(historyFoodEntity: HistoryFoodEntity)

    @Query("SELECT * FROM HistoryFoodEntity ORDER BY scannedDate DESC")
    suspend fun getHistoryFood(): List<HistoryFoodEntity>

    @Query("SELECT category, COUNT(category) FROM HistoryFoodEntity GROUP BY category")
    suspend fun getCategoriesWithCount(): List<CategoryEntity>

    @Delete
    suspend fun deleteHistoryFood(historyFoodEntity: HistoryFoodEntity)
}
