package com.nsu.safefood.data.scanning

import com.nsu.safefood.domain.scanning.BarcodeInfo
import com.nsu.safefood.domain.scanning.BarcodeRepository
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.toDataStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BarcodeRepositoryImpl @Inject constructor(private val barcodeDataSource: BarcodeDataSource) :
    BarcodeRepository {

    override suspend fun getBarcodeInfo(code: Long): DataStatus<BarcodeInfo> {
        return withContext(Dispatchers.IO){
            val response = barcodeDataSource.getBarcodeInfo(code)
            response.toDataStatus()
        }
    }


}

