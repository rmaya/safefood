package com.nsu.safefood.data.history_food

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HistoryFoodDataSourceImpl @Inject constructor(
    private val historyFoodDao: HistoryFoodDao
): HistoryFoodDataSource {

    override suspend fun saveFood(historyFoodEntity: HistoryFoodEntity) {
        historyFoodDao.saveFood(historyFoodEntity)
    }

    override suspend fun getHistoryFood(): List<HistoryFoodEntity> {
        return historyFoodDao.getHistoryFood()
    }

    override suspend fun deleteHistoryFood(historyFoodEntity: HistoryFoodEntity) {
        historyFoodDao.deleteHistoryFood(historyFoodEntity)
    }

    override suspend fun getCategoriesWithCount(): List<CategoryEntity> {
        return historyFoodDao.getCategoriesWithCount()
    }
}