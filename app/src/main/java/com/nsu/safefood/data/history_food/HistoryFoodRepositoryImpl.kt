package com.nsu.safefood.data.history_food

import android.util.Log
import com.nsu.safefood.domain.HistoryFood
import com.nsu.safefood.domain.history_food.HistoryFoodRepository
import com.nsu.safefood.utils.DataStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.sqrt

@Singleton
class HistoryFoodRepositoryImpl @Inject constructor(
    private val historyFoodDataSource: HistoryFoodDataSource
) : HistoryFoodRepository {

    companion object {
        const val MAX_HISTORY_SIZE = 30
        const val LIMIT_SIMILARITY = 0.5
        const val MIN_COUNT_FAV_CATEGORIES = 3
    }

    override suspend fun saveFood(historyFood: HistoryFood) {
        withContext(Dispatchers.IO) {
            val actualHistory = historyFoodDataSource.getHistoryFood()
            val historyFoodEntity = historyFood.toHistoryFoodEntity()
            if (actualHistory.size < MAX_HISTORY_SIZE) {
                historyFoodDataSource.saveFood(historyFoodEntity)
            } else {
                val lastFood = actualHistory[MAX_HISTORY_SIZE - 1]
                historyFoodDataSource.deleteHistoryFood(lastFood)
                historyFoodDataSource.saveFood(historyFoodEntity)
            }
        }
    }

    override suspend fun getHistoryFood(): DataStatus<List<HistoryFood>> {
        return withContext(Dispatchers.IO) {
            val list = historyFoodDataSource.getHistoryFood().map {
                it.toHistoryFood()
            }
            DataStatus.Success(list)
        }
    }

    override suspend fun deleteHistoryFood(historyFood: HistoryFood) {
        withContext(Dispatchers.IO) {
            val historyFoodEntity = historyFood.toHistoryFoodEntity()
            historyFoodDataSource.deleteHistoryFood(historyFoodEntity)
        }
    }

    override suspend fun getFavoriteCategories(): DataStatus<List<String>> {
        return withContext(Dispatchers.IO) {
            val categories = historyFoodDataSource.getCategoriesWithCount()
            val vector: List<Int> = categories.map {
                it.count
            }
            Log.i("HISTORY", "Categories: $categories")
            val norm = countNorm(vector)
            Log.i("HISTORY", "Norm: $norm")
            val favoriteCategories = mutableListOf<String>()
            categories
                .sortedByDescending {
                    it.count
                }.forEach {
                    val normalizedCount = it.count.toDouble() / norm
                    if (normalizedCount >= LIMIT_SIMILARITY || favoriteCategories.size <= MIN_COUNT_FAV_CATEGORIES) {
                        favoriteCategories.add(it.category)
                    }
                }
            Log.i("HISTORY", "Fav categ-s: $favoriteCategories")
            DataStatus.Success(favoriteCategories)
        }
    }

    private fun countNorm(list: List<Int>): Double {
        var norm = 0
        list.forEach {
            norm += it * it
        }
        return sqrt(norm.toDouble())
    }
}