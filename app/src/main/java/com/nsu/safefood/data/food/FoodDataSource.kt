package com.nsu.safefood.data.food

import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.Food
import retrofit2.Response

interface FoodDataSource {

    suspend fun getFoodInfo(name: String): Response<Food>

    suspend fun getAdditives(): Response<List<Additive>>

    suspend fun getRecommendations(categories: List<String>): Response<List<Food>>
}