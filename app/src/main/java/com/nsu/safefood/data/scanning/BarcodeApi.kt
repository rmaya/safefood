package com.nsu.safefood.data.scanning

import com.nsu.safefood.domain.scanning.BarcodeInfo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path


interface BarcodeApi {

    @GET("{code}")
    suspend fun getBarcodeInfo(@Path("code") code: Long): Response<BarcodeInfo>

}