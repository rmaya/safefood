package com.nsu.safefood.data.scanning

import com.nsu.safefood.domain.scanning.BarcodeInfo
import retrofit2.Response


interface BarcodeDataSource {
    suspend fun getBarcodeInfo(code: Long): Response<BarcodeInfo>
}