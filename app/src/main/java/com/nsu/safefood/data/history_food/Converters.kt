package com.nsu.safefood.data.history_food

import android.util.Log
import androidx.room.TypeConverter
import com.nsu.safefood.domain.Additive

class Converters {

    companion object {
        const val MAJOR_DELIMITER = "##"
        const val MINOR_DELIMITER = "$%&"
    }

    @TypeConverter
    fun fromAdditivesToString(additives: List<Additive>): String {
        if (additives.isEmpty()) {
            return ""
        }
        return additives.joinToString(separator = MAJOR_DELIMITER) {
            Log.i("TYPE_CONV", "AdditivesToString: $it")
            it.name + MINOR_DELIMITER + it.code + MINOR_DELIMITER + it.danger
        }

    }

    @TypeConverter
    fun fromStringToAdditives(str: String): List<Additive> {
        if (str.isEmpty()){
            return emptyList()
        }
        val additives = str.split(MAJOR_DELIMITER)
        val list = mutableListOf<Additive>()
        for (element in additives) {
            val additive = element.split(MINOR_DELIMITER)
            list.add(Additive(additive[0], additive[1], additive[2].toInt()))
        }
        return list
    }
}