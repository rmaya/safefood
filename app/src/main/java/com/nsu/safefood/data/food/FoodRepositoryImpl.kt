package com.nsu.safefood.data.food

import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.Food
import com.nsu.safefood.domain.food.FoodRepository
import com.nsu.safefood.utils.DataStatus
import com.nsu.safefood.utils.toDataStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FoodRepositoryImpl @Inject constructor(
    private val dataSource: FoodDataSource
) : FoodRepository {

    override suspend fun getFoodInfo(name: String): DataStatus<Food> {
        return withContext(Dispatchers.IO) {
            val response = dataSource.getFoodInfo(name = name)
            response.toDataStatus()
        }
    }

    override suspend fun getAdditives(): DataStatus<List<Additive>> {
        return withContext(Dispatchers.IO){
            val response = dataSource.getAdditives()
            response.toDataStatus()
        }
    }

    override suspend fun getRecommendations(categories: List<String>): DataStatus<List<Food>> {
        return withContext(Dispatchers.IO){
            val response = dataSource.getRecommendations(categories)
            response.toDataStatus()
        }
    }
}