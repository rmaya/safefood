package com.nsu.safefood.data.food

import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.Food
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface FoodApi {

    @GET("/product/{name}")
    suspend fun getFoodInfo(@Path("name") name: String): Response<Food>

    @GET("/additives")
    suspend fun getAdditives(): Response<List<Additive>>

    @POST("/recommendations")
    suspend fun getRecommendations(@Body categories: List<String>): Response<List<Food>>
}