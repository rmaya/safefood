package com.nsu.safefood.data.history_food

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
data class CategoryEntity(
    val category: String,
    @ColumnInfo(name = "COUNT(category)")
    val count: Int
)