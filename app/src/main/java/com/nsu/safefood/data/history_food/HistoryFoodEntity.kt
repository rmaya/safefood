package com.nsu.safefood.data.history_food

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.HistoryFood

@Entity(tableName = "HistoryFoodEntity")
data class HistoryFoodEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,

    val name: String,
    val rating: Double,
    val category: String,
    val image: String,
    val scannedDate: String,
    val ingredients: String,

    val additives: List<Additive>
)

fun HistoryFoodEntity.toHistoryFood(): HistoryFood{
    return HistoryFood(
        name = this.name,
        rating = this.rating,
        category = this.category,
        image = this.image,
        ingredients = this.ingredients,
        additives = this.additives,
        scannedDate = this.scannedDate
    )
}

fun HistoryFood.toHistoryFoodEntity(): HistoryFoodEntity{
    return HistoryFoodEntity(
        name = this.name,
        rating = this.rating,
        category = this.category,
        image = this.image,
        ingredients = this.ingredients,
        additives = this.additives,
        scannedDate = this.scannedDate
    )
}