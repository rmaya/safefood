package com.nsu.safefood.di

import javax.inject.Qualifier

@Qualifier
annotation class FoodQualifier

@Qualifier
annotation class BarcodeQualifier