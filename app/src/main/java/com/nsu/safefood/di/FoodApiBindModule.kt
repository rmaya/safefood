package com.nsu.safefood.di

import com.nsu.safefood.data.food.FoodDataSource
import com.nsu.safefood.data.food.FoodDataSourceImpl
import com.nsu.safefood.data.food.FoodRepositoryImpl
import com.nsu.safefood.domain.food.FoodRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface FoodApiBindModule {

    @Binds
    @Singleton
    @Suppress("FunctionName")
    fun bindFoodDataSourceImpl_to_foodDataSource(impl: FoodDataSourceImpl): FoodDataSource

    @Binds
    @Singleton
    @Suppress("FunctionName")
    fun bindFoodRepositoryImpl_to_foodRepository(impl: FoodRepositoryImpl): FoodRepository
}