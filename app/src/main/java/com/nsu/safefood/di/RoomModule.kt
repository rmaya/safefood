package com.nsu.safefood.di

import android.content.Context
import androidx.room.Room
import com.nsu.safefood.data.history_food.HistoryFoodDao
import com.nsu.safefood.data.history_food.HistoryFoodDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Singleton
    @Provides
    fun provideHistoryFoodDatabase(@ApplicationContext context: Context): HistoryFoodDatabase {
        return Room.databaseBuilder(
            context,
            HistoryFoodDatabase::class.java, "history_food_database"
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideHistoryFoodDao(historyFoodDatabase: HistoryFoodDatabase): HistoryFoodDao =
        historyFoodDatabase.getHistoryFoodDao()
}