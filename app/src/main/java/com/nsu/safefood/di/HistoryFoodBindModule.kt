package com.nsu.safefood.di

import com.nsu.safefood.data.history_food.HistoryFoodDataSource
import com.nsu.safefood.data.history_food.HistoryFoodDataSourceImpl
import com.nsu.safefood.data.history_food.HistoryFoodRepositoryImpl
import com.nsu.safefood.domain.history_food.HistoryFoodRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface HistoryFoodBindModule {

    @Binds
    @Singleton
    @Suppress("FunctionName")
    fun bindHistoryFoodDataSourceImpl_to_HistoryFoodDataSource(historyFoodDataSourceImpl: HistoryFoodDataSourceImpl):
            HistoryFoodDataSource

    @Binds
    @Singleton
    @Suppress("FunctionName")
    fun bindHistoryFoodRepositoryImpl_to_HistoryFoodRepository(historyFoodRepositoryImpl: HistoryFoodRepositoryImpl):
            HistoryFoodRepository


}