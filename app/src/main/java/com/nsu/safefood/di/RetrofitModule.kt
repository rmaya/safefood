package com.nsu.safefood.di

import com.nsu.safefood.data.NoConnectionInterceptor
import com.nsu.safefood.data.food.FoodApi
import com.nsu.safefood.data.scanning.BarcodeApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RetrofitModule {

    @Singleton
    @Provides
    @BarcodeQualifier
    fun provideBarCodeRetrofit(noConnectionInterceptor: NoConnectionInterceptor): Retrofit =
        Retrofit.Builder()
            .client(
                OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().also {
                    it.level = HttpLoggingInterceptor.Level.BODY
                })
                    .addInterceptor(noConnectionInterceptor)
                    .connectTimeout(3, TimeUnit.MINUTES)
                    .writeTimeout(3, TimeUnit.MINUTES)
                    .readTimeout(3, TimeUnit.MINUTES)
                    .build()
            )
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://barcode.monster/api/")
            .build()

    @Singleton
    @Provides
    @FoodQualifier
    fun provideFoodRetrofit(noConnectionInterceptor: NoConnectionInterceptor): Retrofit =
        Retrofit.Builder()
            .client(
                OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().also {
                    it.level = HttpLoggingInterceptor.Level.BODY
                })
                    .addInterceptor(noConnectionInterceptor)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(3, TimeUnit.MINUTES)
                    .readTimeout(3, TimeUnit.MINUTES)
                    .build()
            )
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://192.168.0.7:8080/")
            .build()


    @Singleton
    @Provides
    fun provideBarcodeApi(@BarcodeQualifier retrofit: Retrofit): BarcodeApi =
        retrofit.create(BarcodeApi::class.java)

    @Singleton
    @Provides
    fun provideFoodApi(@FoodQualifier retrofit: Retrofit): FoodApi = retrofit.create(FoodApi::class.java)
}

