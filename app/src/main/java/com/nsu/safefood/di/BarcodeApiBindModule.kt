package com.nsu.safefood.di

import com.nsu.safefood.data.scanning.BarcodeDataSource
import com.nsu.safefood.data.scanning.BarcodeRemoteDataSource
import com.nsu.safefood.data.scanning.BarcodeRepositoryImpl
import com.nsu.safefood.domain.scanning.BarcodeRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface BarcodeApiBindModule {

    @Binds
    @Singleton
    @Suppress("FunctionName")
    fun bindBarcodeRemoteDataSourceImpl_to_barcodeDataSource(impl: BarcodeRemoteDataSource): BarcodeDataSource

    @Binds
    @Singleton
    @Suppress("FunctionName")
    fun bindBarcodeRepositoryImpl_to_barcodeRepository(impl: BarcodeRepositoryImpl): BarcodeRepository
}