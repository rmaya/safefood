package com.nsu.safefood.domain.food

import com.nsu.safefood.domain.Food
import com.nsu.safefood.utils.DataStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetFoodInfoUseCase @Inject constructor(
    private val foodRepository: FoodRepository
){

    suspend operator fun invoke(name: String): DataStatus<Food>{
        return foodRepository.getFoodInfo(name = name)
    }
}