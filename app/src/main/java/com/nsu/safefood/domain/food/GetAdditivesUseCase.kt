package com.nsu.safefood.domain.food

import com.nsu.safefood.domain.Additive
import com.nsu.safefood.utils.DataStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetAdditivesUseCase @Inject constructor(
    private val foodRepository: FoodRepository
){
    suspend operator fun invoke(): DataStatus<List<Additive>> {
        return foodRepository.getAdditives()
    }
}