package com.nsu.safefood.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Additive(
    val name: String,
    val code: String,
    val danger: Int,
    val general: String = "",
    val benefit: String = "",
    val harm: String = ""
): Parcelable


