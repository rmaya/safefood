package com.nsu.safefood.domain.history_food

import com.nsu.safefood.domain.HistoryFood
import com.nsu.safefood.utils.DataStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetHistoryFoodUseCase @Inject constructor(
    private val historyFoodRepository: HistoryFoodRepository
){
    suspend operator fun invoke(): DataStatus<List<HistoryFood>> {
        return historyFoodRepository.getHistoryFood()
    }
}