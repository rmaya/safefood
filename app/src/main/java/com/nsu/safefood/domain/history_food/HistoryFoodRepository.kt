package com.nsu.safefood.domain.history_food

import com.nsu.safefood.domain.HistoryFood
import com.nsu.safefood.utils.DataStatus

interface HistoryFoodRepository {

    suspend fun saveFood(historyFood: HistoryFood)

    suspend fun getHistoryFood(): DataStatus<List<HistoryFood>>

    suspend fun deleteHistoryFood(historyFood: HistoryFood)

    suspend fun getFavoriteCategories(): DataStatus<List<String>>
}