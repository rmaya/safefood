package com.nsu.safefood.domain.food

import com.nsu.safefood.domain.Additive
import com.nsu.safefood.domain.Food
import com.nsu.safefood.utils.DataStatus

interface FoodRepository {

    suspend fun getFoodInfo(name: String): DataStatus<Food>

    suspend fun getAdditives(): DataStatus<List<Additive>>

    suspend fun getRecommendations(categories: List<String>): DataStatus<List<Food>>
}