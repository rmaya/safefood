package com.nsu.safefood.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Food(
    val imageUrl: String,
    val name: String,
    val category: String,
    val ingredients: String,
    val additives: List<Additive>,
    val rating: Double
) : Parcelable