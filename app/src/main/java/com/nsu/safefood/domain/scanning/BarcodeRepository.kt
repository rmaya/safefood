package com.nsu.safefood.domain.scanning

import com.nsu.safefood.utils.DataStatus

interface BarcodeRepository {
    suspend fun getBarcodeInfo(code: Long): DataStatus<BarcodeInfo>
}