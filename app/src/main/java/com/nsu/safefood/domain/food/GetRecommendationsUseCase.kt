package com.nsu.safefood.domain.food

import com.nsu.safefood.domain.Food
import com.nsu.safefood.utils.DataStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetRecommendationsUseCase @Inject constructor(
    private val foodRepository: FoodRepository
){

    suspend operator fun invoke(categories: List<String>): DataStatus<List<Food>> {
        return foodRepository.getRecommendations(categories)
    }
}