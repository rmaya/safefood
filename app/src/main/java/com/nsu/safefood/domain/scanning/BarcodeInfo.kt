package com.nsu.safefood.domain.scanning

data class BarcodeInfo(
    val `class`: String,
    val code: String,
    val company: String?,
    val description: String?,
    val image_url: String?
)

