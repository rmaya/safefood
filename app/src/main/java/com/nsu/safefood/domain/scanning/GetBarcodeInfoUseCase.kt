package com.nsu.safefood.domain.scanning

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetBarcodeInfoUseCase @Inject constructor(private val barcodeRepository: BarcodeRepository) {
    suspend operator fun invoke(code: Long) = barcodeRepository.getBarcodeInfo(code)
}