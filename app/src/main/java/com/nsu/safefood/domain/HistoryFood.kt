package com.nsu.safefood.domain

import java.text.SimpleDateFormat
import java.util.*

data class HistoryFood(
    val name: String,
    val rating: Double,
    val category: String,
    val image: String,
    val scannedDate: String,
    val ingredients: String,
    val additives: List<Additive>
)

fun Food.toHistoryFood(): HistoryFood {
    val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    val currentDate = formatter.format(Date())
    return HistoryFood(
        name = this.name,
        image = this.imageUrl,
        category = this.category,
        rating = this.rating,
        scannedDate = currentDate,
        ingredients = this.ingredients,
        additives = this.additives
    )
}

fun HistoryFood.toFood(): Food {
    return Food(
        name = this.name,
        imageUrl = this.image,
        ingredients = this.ingredients,
        category = this.category,
        additives = this.additives,
        rating = this.rating
    )
}