package com.nsu.safefood.domain.history_food

import com.nsu.safefood.domain.HistoryFood
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SaveFoodUseCase @Inject constructor(
    private val historyFoodRepository: HistoryFoodRepository
){
    suspend operator fun invoke(historyFood: HistoryFood) {
        historyFoodRepository.saveFood(historyFood)
    }
}