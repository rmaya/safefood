package com.nsu.safefood.utils.errors_processing

enum class ErrorCode {
    UNAUTHORIZED,
    FORBIDDEN,
    NOT_FOUND,
    UNKNOWN,
    SOCKET_TIMEOUT,
    UNKNOWN_HOST,
    NO_CONNECTIVITY,
    NO_INTERNET,
    NO_BAR_CODE,
    NO_BAR_CODE_INFO,
    CAN_NOT_PROCESS_BARCODE
}